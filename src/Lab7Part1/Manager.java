
package Lab7Part1;


public class Manager extends Employee{
    private double bonus;
    
public Manager(){}

public Manager(String name, double hours, double wage, double bonus){
    super(name,hours,wage);
    this.bonus = bonus;
}


double calculatePay(){ 
 return super.calculatePay() + bonus;
}
}
