
package Lab7Part1;


public class Employee {
        private String name;
        protected double hours;
        protected double wage;
        
public Employee(){}

public Employee(String name, double hours, double wage){
    this.name = name;
    this.hours = hours;
    this.wage = wage;
}


double calculatePay(){
    return this.hours * this.wage;
}
}
