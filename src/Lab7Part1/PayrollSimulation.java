package Lab7Part1;

public class PayrollSimulation {
   
    public static void main(String[] args) {
        Employee e1 = new Employee("Mark", 80.0, 22.20);
        Manager e2 = new Manager("Lisa", 80.0, 23.50, 100.00);
        
        
        System.out.println("Marks paycheck: " + e1.calculatePay());
        System.out.println("Lisa's paycheck: " + e2.calculatePay());
        
              
    }
    
}
